package es.d2d.jpa.apptest.dto;

import es.d2d.beanwrapper.dto.DatabaseClass;

public enum Enabled implements DatabaseClass<Boolean> {
    ENABLED, DISABLED;

    @Override
    public Boolean getDatabaseValue() {
	return this == ENABLED;
    }

}
