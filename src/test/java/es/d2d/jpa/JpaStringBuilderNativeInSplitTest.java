package es.d2d.jpa;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import es.d2d.jpa.apptest.TestApplication;
import es.d2d.jpa.apptest.domain.Person;
import es.d2d.jpa.apptest.dto.PersonDTO;
import es.d2d.jpa.apptest.repositories.PersonRepository;

public class JpaStringBuilderNativeInSplitTest extends TestApplication {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private PersonRepository personRepository;

    @Before
    public void init() {
	Person person = new Person();
	person.setId(1L);
	person.setName("Luis");
	person.setSurname("Labiano");
	person.setAge(11);
	person.setHeight(1.20f);
	person.setBirthday(Calendar.getInstance());
	person.setCreationDate(Calendar.getInstance().getTime());
	person.setEnabled(true);
	person.setAlive(true);
	personRepository.save(person);
    }

    @Test
    public void personDTOResultTest() {
	int l = 1005;
	List<Long> ids = new ArrayList<Long>(l);
	while (l-- > 0) {
	    ids.add((long) l);
	}
	JpaStringBuilder jpa = JpaStringBuilder.createNative()
		.select("p.id_person as idPerson")
		.from("Person p")
		.where("p.id_person in (:list) or p.id_person > 1", ids);
	TypedQuery<PersonDTO> query = jpa.createQuery(entityManager, PersonDTO.class);
	List<PersonDTO> persons = query.getResultList();

	assertThat(persons, hasSize(1));
	PersonDTO person = persons.get(0);
	assertThat(person, is(not(nullValue())));
	assertThat(person.getIdPerson(), is(not(nullValue())));
    }
}
