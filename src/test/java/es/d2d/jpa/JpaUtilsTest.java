package es.d2d.jpa;

import static org.junit.Assert.assertThat;

import org.junit.Test;

import static org.hamcrest.Matchers.is;

import es.d2d.jpa.utils.JpaUtils;

public class JpaUtilsTest {

    @Test
    public void replaceTest() {
	StringBuilder sb1 = new StringBuilder("(:jpa) (:jpa)");
	JpaUtils.replaceStringBuilder(sb1, ":jpa", "select 1 from dual");
	assertThat(sb1.toString(), is("(select 1 from dual) (select 1 from dual)"));

	StringBuilder sb2 = new StringBuilder("(:jpa1) (:jpa2)");
	JpaUtils.replaceStringBuilder(sb2, ":jpa1", "select 1 from dual");
	JpaUtils.replaceStringBuilder(sb2, ":jpa2", "select 2 from dual");
	assertThat(sb2.toString(), is("(select 1 from dual) (select 2 from dual)"));

	StringBuilder sb3 = new StringBuilder("(:jpa)");
	JpaUtils.replaceStringBuilder(sb3, ":jpa", "select 1 from (:jpa)");
	assertThat(sb3.toString(), is("(select 1 from (:jpa))"));
    }

}
