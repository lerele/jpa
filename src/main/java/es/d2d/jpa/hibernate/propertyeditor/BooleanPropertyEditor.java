package es.d2d.jpa.hibernate.propertyeditor;

import org.apache.commons.lang3.ArrayUtils;

import es.d2d.beanwrapper.propertyeditor.IPropertyEditor;

public class BooleanPropertyEditor implements IPropertyEditor {

    private String[] falseValues = { "FALSE", "0", "F", "NULL", "NIL", "" }; // Any other String returns true

    public Object getValue(Object value) {
	if (value instanceof Number) {
	    return ((Number) value).intValue() == 1;
	} else if (value instanceof Boolean) {
	    return (Boolean) value;
	} else if (value instanceof CharSequence) {
	    String seq = ((CharSequence) value).toString().toUpperCase();
	    return !ArrayUtils.contains(falseValues, seq);
	}
	return null;
    }

    public void setFalseValues(String... values) {
	falseValues = values;
    }
}
