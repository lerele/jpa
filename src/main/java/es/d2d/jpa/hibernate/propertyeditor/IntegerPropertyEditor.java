package es.d2d.jpa.hibernate.propertyeditor;

import es.d2d.beanwrapper.propertyeditor.IPropertyEditor;

public class IntegerPropertyEditor implements IPropertyEditor {

    public Object getValue(Object value) {
	if (value instanceof Number) {
	    return ((Number) value).intValue();
	}
	return null;
    }
}
