package es.d2d.jpa.hibernate.propertyeditor;

import java.util.Calendar;
import java.util.Date;

import es.d2d.beanwrapper.propertyeditor.IPropertyEditor;

public class CalendarPropertyEditor implements IPropertyEditor {

    public Object getValue(Object value) {
	if (value instanceof Calendar) {
	    return (Calendar) value;
	} else if (value instanceof Date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime((Date) value);
	    return cal;
	}
	return null;
    }
}
