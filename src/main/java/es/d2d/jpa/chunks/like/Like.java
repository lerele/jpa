package es.d2d.jpa.chunks.like;

public interface Like {

    String getStatement();

    String getValue();

}
