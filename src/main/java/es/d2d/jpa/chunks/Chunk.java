package es.d2d.jpa.chunks;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import es.d2d.jpa.JpaStringBuilder;
import es.d2d.jpa.chunks.like.Like;
import es.d2d.jpa.exceptions.ParseJpaException;
import es.d2d.jpa.utils.ConditionValues;
import es.d2d.jpa.utils.ConditionValuesParser;
import es.d2d.jpa.utils.JpaUtils;

public abstract class Chunk {

    protected JpaStringBuilder jpa;

    protected StringBuilder sb;

    private List<Parameter<?>> params;
    public List<Parameter<JpaStringBuilder>> jpas;

    public Chunk(JpaStringBuilder jpa) {
	this.jpa = jpa;
	sb = new StringBuilder();
	params = new ArrayList<Parameter<?>>();
	jpas = new ArrayList<Parameter<JpaStringBuilder>>();
    }

    protected String extractParams(String str, Object... values) {
	int args = values == null ? 0 : values.length;
	if (args == 0) {
	    return str;
	}
	if (StringUtils.isBlank(str)) {
	    return str;
	}
	if (args != StringUtils.countMatches(str, ":")) {
	    throw new ParseJpaException("El número de parámetros no coincide en el predicado " + str);
	}
	ConditionValues cv = ConditionValuesParser.getInstance(str, values).parse();
	str = cv.getCondition();
	values = cv.getValues();

	Matcher m = Pattern.compile(":([^\\s\\)]+)").matcher(str);
	int i = 0;
	while (m.find()) {
	    String paramName = m.group(1);
	    Object value = values[i++];
	    if (value instanceof JpaStringBuilder) {
		jpas.add(new Parameter<JpaStringBuilder>(paramName, (JpaStringBuilder) value));
		continue;
	    }
	    if (value != null && value.getClass().isArray()) {
		value = JpaUtils.arrayToList(value); // convert array to list
	    }
	    params.add(new Parameter<Object>(paramName, value));
	}
	return str;
    }

    public boolean isNotEmpty() {
	return sb.length() > 0;
    }

    public List<Parameter<?>> getParams() {
	return params;
    }

    public List<Parameter<JpaStringBuilder>> getJpas() {
	return jpas;
    }

    public static final String trim(String str) {
	return StringUtils.trim(str);
    }

    public abstract String build();

    public JpaStringBuilder and(String and, Object... values) {
	and = extractParams(and, values);
	and = trim(and);
	if (this.sb.length() != 0) {
	    this.sb.append(" and ");
	}
	this.sb.append(and);
	return jpa;
    }

    public JpaStringBuilder andlike(Like like) {
	if (like.getValue() == null)
	    return jpa;
	// REGEXP_LIKE es de oracle
	// return and(" REGEXP_LIKE(" + alias + ", " + var + " )", value.replaceAll("(.)", "[[=$1=]]"));
	return and(like.getStatement(), like.getValue());
    }

}
