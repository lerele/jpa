package es.d2d.jpa.chunks;

import es.d2d.jpa.JpaStringBuilder;

public class Where extends Chunk {

    public Where(JpaStringBuilder jpa) {
	super(jpa);
    }

    @Override
    public String build() {
	return " where " + sb.toString();
    }

}
