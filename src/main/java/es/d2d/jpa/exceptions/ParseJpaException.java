package es.d2d.jpa.exceptions;

public class ParseJpaException extends RuntimeException {
    public static final long serialVersionUID = -1;

    public ParseJpaException(String message) {
	super(message);
    }
}